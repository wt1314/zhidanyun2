# 基于SpringBoot 框架，互联互通协议+云快充协议1.5协议+新能源汽车+二轮车+公交车-充电源代码

#### Description
充电桩系统平台完整源代码；①技术：pring boot + mybatis +Sqlserver<Mysql>、Redis、Netty、时序数据库、MQTT。协议：云快充1.5、1.6 协议、互联互通协议、分时计费。源码无加密，稳定运行；
源码包含端微信小程序源码、管理平台源码、互联互通接口、数据库部分；

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
